package com.cyance;

import java.util.concurrent.TimeUnit;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import com.cyance.pages.LoginPage;
import com.cyance.pages.NexusDashboardPage;
import com.cyance.helpers.SettingsHelper;

public class LoginTest {

	private static WebDriver driver;
	private NexusDashboardPage nexusDashboardPage;
	
	@Before
	public void setup(){
		System.setProperty("webdriver.chrome.driver","C:\\Users\\qamon\\eclipse-workspace\\UITests\\src\\main\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	    SettingsHelper.loadProperties();
	    driver.get(SettingsHelper.url);
	   }
	
	@Test
	public void login() throws InterruptedException {
		LoginPage loginPage = new LoginPage(driver);
		nexusDashboardPage = loginPage.loginAs(SettingsHelper.adminUsername, SettingsHelper.adminPassword);
		Thread.sleep(3000);
		nexusDashboardPage.openLeftMenu();
	}
}
