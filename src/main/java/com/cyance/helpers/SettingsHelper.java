package com.cyance.helpers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class SettingsHelper {	
	
	public static String fileName, env, url, adminUsername, adminPassword, nexusUsername, nexusPassword, contactIntelUsername, contactIntelPassword;
	private static Properties properties;
	
	public static void loadEnv() {
		try {
			properties = new Properties();
			File file = new File("target\\classes\\maven.properties");
			FileInputStream fileInputStream = new FileInputStream(file);
			properties.load(fileInputStream);
			fileInputStream.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		env = properties.getProperty("app.environment");
	}
	
	public static void loadProperties() {
		
		loadEnv();
		
		if(env.equals("qa"))
			fileName = "qa.properties";
		else if(env.equals("prod"))
			fileName = "prod.properties";
		else if(env.equals("dev"))
			fileName = "dev.properties";
		
		try {
			properties = new Properties();
			File file = new File("src\\main\\resources\\properties\\" + fileName);
			FileInputStream fileInputStream = new FileInputStream(file);
			properties.load(fileInputStream);
			fileInputStream.close();
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		url = properties.getProperty("url");
		adminUsername = properties.getProperty("admin.username");
		adminPassword = properties.getProperty("admin.password");
		nexusUsername = properties.getProperty("nexus.username");
		nexusPassword = properties.getProperty("nexus.password");
		contactIntelUsername = properties.getProperty("contactintel.username");
		contactIntelPassword = properties.getProperty("contactintel.password");
	}
}