package com.cyance.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.cyance.pages.LoginPage;

public class LeftMenu {
    
	private WebDriver driver;
	
	private By logout = By.xpath("//span[contains(text(),'Sign out')]");
    
    public LeftMenu(WebDriver driver) {
        this.driver = driver;
    }
	
	public LoginPage logout() {
		driver.findElement(logout).click();
		return new LoginPage(driver);
	}

}