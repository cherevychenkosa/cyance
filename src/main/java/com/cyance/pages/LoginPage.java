package com.cyance.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

	private WebDriver driver;
	private By usernameField = By.id("username");
	private By passwordField = By.id("password");
	private By loginBtn = By.cssSelector("form button");
    
    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }
    
    public LoginPage typeUsername(String username) {
    	driver.findElement(usernameField).sendKeys(username);
    	return new LoginPage(driver);
    }
    
    public LoginPage typePassword(String password) {
    	driver.findElement(passwordField).sendKeys(password);
    	return new LoginPage(driver);
    }
    
    public LoginPage submitLoginExceptionFailure() {
    	driver.findElement(loginBtn).submit();
    	return new LoginPage(driver);
    }
    
    public NexusDashboardPage loginAs(String username, String password) {
    	typeUsername(username);
    	typePassword(password);
    	driver.findElement(loginBtn).submit();
    	return new NexusDashboardPage(driver);
    }
    
}