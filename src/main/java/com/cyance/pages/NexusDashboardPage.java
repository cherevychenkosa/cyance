package com.cyance.pages;

//import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.cyance.components.LeftMenu;

public class NexusDashboardPage extends BasePage {

//	private WebDriver driver;
//	private By resetAllFiltersBtn = By.className("btn btn-primary btn-nexus-orange");
    
    public NexusDashboardPage(WebDriver driver) {
    	super(driver);
        this.leftMenu = new LeftMenu(driver);
    }
    
}
