package com.cyance.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.cyance.components.LeftMenu;

public class BasePage {
    
	protected WebDriver driver;
	private By hamburgerIcon = By.className("nav-link");
	private By crossIcon = By.className("close-sidebar");
	protected LeftMenu leftMenu;
	
    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.leftMenu = new LeftMenu(driver);
    }
    
    public boolean isLeftMenuOpen() {
		String classes = driver.findElement(By.cssSelector("body")).getAttribute("class");
	    for (String c : classes.split(" ")) {
	        if (c.equals("layout-collapsed")) {
	            return true;
	        }
	    }
		return false;
	}
	
	public void openLeftMenu( ) {
		if(!isLeftMenuOpen()) {
			driver.findElement(hamburgerIcon).click();
		}
	}
	
	public void closeLeftMenu( ) {
		if(isLeftMenuOpen()) {
			driver.findElement(crossIcon).click();
		}
	}
        
    public LoginPage logout() {
    	openLeftMenu();
    	return leftMenu.logout();
    }
    
}